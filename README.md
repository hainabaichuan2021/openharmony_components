![输入图片说明](https://images.gitee.com/uploads/images/2021/0908/181516_642573c7_1850385.png "仓库banner1100_90.png")
OpenHarmony 是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目，目标是面向全场景、全连接、全智能时代，基于开源的方式，搭建一个智能终端设备操作系统的框架和平台，促进万物互联产业的繁荣发展。  
此次大赛旨在鼓励开发者了解 OpenHarmony 并积极参与基于 OpenHarmony 的三方的 js 组件开发，贡献优秀的创新型开源组件作品，为  OpenHarmony 注入源源不断的活力，将  OpenHarmony  真正玩转起来！Let‘s have fun together！

-[大赛官网](https://gitee.com/openharmony-2021/#/)

#### 活动流程

1、主赛场

| 赛程  | 时间  |
|---|---|
| 报名及作品提交  | 7月20日-9月20日  |
| 赛前辅导（直播）  | 8月24日-9月17日  |
| 入围作品公布  | 9月23日  |
| 大众投票  | 9月23日-9月30日  |
| 评委评分 | 9月23日-9月30日  |
| 结果公布  | 10月11日  |

2、[征文赛场](https://gitee.com/gitee-community/OHZW210809)

| 赛程  | 时间  |
|---|---|
| 报名及投稿  | 8月11日-10月20日  |
| 评审时间 | 10月21日-11月3日  |
| 结果公布  | 11月8日-11月12日  |


#### 作品范围
基于 OpenHarmony 开发三方的js组件，不限类型（可包含已有组件迁移）  
例如：基础组件、容器组件、媒体组件、画布组件  

#### 大赛资料
1. 参赛必读文档
- [quick-start](https://gitee.com/openharmony/docs/tree/master/zh-cn/application-dev/quick-start)
- [快速入门讲解](https://gitee.com/isrc_ohos/ultimate-harmony-reference/blob/master/OpenHarmony%20JS%20Demo%E5%BC%80%E5%8F%91%E8%AE%B2%E8%A7%A3.md)  
- [项目配置流程](https://gitee.com/isrc_ohos/ultimate-harmony-reference/blob/master/OpenHarmony%20JS%E9%A1%B9%E7%9B%AE%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B.md)
2. 参考 Demo 
- [JS Demo开发示例](https://gitee.com/isrc_ohos/open-harmony-js-demos)    
3. 视频教程指引  
- [视频教程](http://mtw.so/6oLJOt)
4. 赛前辅导（直播）
 - （8月24日）[OpenHarmony如何开发JS UI组件（回放）](https://harmonyos.51cto.com/activity/89#gitee)
 - （9月01日）[基于OpenHarmony的JS开发基础](https://harmonyos.51cto.com/activity/91#gitee)

 -[直播详情>>>](https://gitee.com/gitee-community/openharmony_components/blob/master/赛事辅导锦囊/赛前辅导（直播）详情.md)

#### 作品要求
本次作品应符合主题方向，且参赛者须对参赛作品拥有自主知识产权，采用开放源码的部分，也必须在理解、消化、吸收基础上加以引用，做到安全、自主、可控，一旦发现抄袭，剽窃其他项目作品并经核实后，将取消参与资格。  

#### 参赛流程
1. 点击「[报名表单](https://jinshuju.net/f/jgSRSX)」，完成报名  
2. 扫码添加“Gitee 助手菌”为好友，回复「OH+报名时所填写的姓名」进群👇 （需完成报名后才可进群）   
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/154958_25eb6c0f_6506560.png "小助手企业微信-1.png") 

3. 开发组件，编写规范文档、帮助文档（包含组件/应用介绍、效果展示、代码模块说明等）
4. 将组件代码及相关文档上传至到 Gitee 仓库（在 Gitee 创建公开作品仓库）    

 **👉🏻 仓库要求**       
- 作品仓库命名：“作品名”，长度不得超过 50 个字符  
- 参赛项目指定为开源项目，代码遵循木兰2.0许可证进行开源  
- 描述文档必须要有完整详细的描述，最好可以包括截图等  
5. 完成作品以后，所有作品将以提交 issue 的方式，将作品需提交至大赛仓库，点击[此处](https://gitee.com/gitee-community/openharmony_components/issues/I3ZDBV?from=project-issue)提交    
  
 **🌟注意：** issue 名为项目名，issue 内容需要按照模板的规则进行填写


#### 奖项设置
- 一等奖（1名）：10000 元奖金+开发板 * 1 +《鸿蒙操作系统应用开发实践》书籍 * 1
- 二等奖（3名）：5000 元奖金+开发板 * 1 +《鸿蒙操作系统应用开发实践》书籍 * 1
- 三等奖（5名）：1000 元奖金+开发板 * 1
- 人气作品奖（10 名）：「鸿蒙+Gitee 」周边大礼包
- 阳光普照奖：所有入围作品可获得文化衫一件  

🌟**注：** 以上全部奖金均为税前金额，由主办方代扣代缴个人所得税  
  
#### 作品评审  
本次评分由评委打分，满分为 100 分，由主委会评委根据作品的创意性、实用性、用户体验、代码规范等四个维度点评打分 
|评选维度 | 说明  | 分值  |
|---|---|---|
|创意性   | 作品的创新程度  | 30%  |
|实用性   | 作品在应用场景中的实际应用程度  | 30%  |
|用户体验   | 用户体验价值，用户能够轻松使用组件，并获得良好体验感  | 25%  |
|代码规范| 代码的质量，美观度，是否符合规范  |15%   |
  
**人气评分**   
9月3日-9月11日，将开启大众投票通道，通过投票评出人气作品，人气最高的 10 份作品的作者将获得 Gitee 周边大礼包一份。   
  
#### 常见问题
1. 代码必须开源吗？
是的，我们鼓励协力合作，为社会贡献，让创意和代码更充分地发挥社会价值。  
2. 如何联系主办方，获取大赛动态信息？  
小助手微信： gitee2013 ，备注「活动咨询」，随时答疑，获取活动态信息。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/154912_d1aac93c_6506560.png "小助手企业微信-1.png") 

[![有奖征文](https://images.gitee.com/uploads/images/2021/0813/135514_bde0cf79_1850385.png "征文活动仓库banner1100_90.png")](https://gitee.com/gitee-community/OHZW210809)

### 有奖征文

为了帮助大赛参赛者和更多开发者，更加全面的了解 OpenHarmony，本次大赛除提供必要的学习文档和视频辅导（直播）外，还将举行“OpenHarmony 组件开发大赛 | 有奖征文”活动。

大家赶快来参加「OpenHarmony组件开发大赛|有奖征文」活动吧！参与投稿还有千元现金奖励、开发板等奖励等你哦！

[了解详情>>>](https://gitee.com/gitee-community/OHZW210809)
